import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Copyright from "../share/copy-right";
import { useForm } from "react-hook-form";
import CTextField from "@/components/atom/c-text-field";
import { AnyObject } from "@/common/type";
import { formUtils } from "@/utils/form-utils";
import CPasswordTextField from "@/components/share/c-password-text-field";
import CEmailTextField from "@/components/share/c-email-text-field";
import { object } from "yup";
import { validationSchemaUtils } from "@/common/validation";
import { useYupValidationResolver } from "@/common/hooks";
import { setTokenToCookies } from "@/common/auth";
import { useRouter } from "next/router";
import { omit } from "lodash";

export default function SignUp() {
  const validationSchema = object({
    email: validationSchemaUtils.email,
    password: validationSchemaUtils.password,
    rePassword: validationSchemaUtils.rePassword,
  });
  const resolver = useYupValidationResolver(validationSchema);
  const { control, handleSubmit } = useForm({ resolver });
  const { push } =  useRouter();

  const onSubmit = (data: AnyObject) => {
    formUtils.submitForm(data, {
      method: "POST",
      endpoint: "/signup",
      onGotSuccess: response => {
        setTokenToCookies(response.data);
        push("/");
      },
      notify: true,
      successMessage: "Đăng ký thành công",
      modifyDataBeforeSubmit: formValue => {
        return omit(formValue, "rePassword");
      },
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box sx={{ marginTop: 8,display: "flex",flexDirection: "column",alignItems: "center" }}>
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Đăng ký
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit(onSubmit)} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <CTextField {...{
                name: "firstName",
                control,
                rules: { required: true },
                componentProps: {
                  autoFocus: true,
                  fullWidth: true,
                  autoComplete: "given-name",
                  id: "firstName",
                  label: "Họ và tên đệm",
                  required: true,
                },
              }} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <CTextField {...{
                name: "lastName",
                control,
                rules: { required: true },
                componentProps: {
                  autoFocus: true,
                  fullWidth: true,
                  autoComplete: "given-name",
                  id: "lastName",
                  label: "Tên",
                  required: true,
                },
              }} />
            </Grid>
            <Grid item xs={12}>
              <CEmailTextField control={control} />
            </Grid>
            <Grid item xs={12}>
              <CPasswordTextField control={control} />
            </Grid>
            <Grid item xs={12}>
              <CPasswordTextField componentProps={{ label: "Nhập lại mật khẩu" }} name="rePassword" control={control} />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >Đăng nhập</Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/signin" variant="body2">Bạn đã có tài khoản? Đăng nhập</Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Copyright sx={{ mt: 5 }} />
    </Container>
  );
}