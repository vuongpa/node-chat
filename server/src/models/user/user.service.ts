import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { RequestCreateUserDto } from "./user.dto";
import { User, UserDocument } from "./user.schema";

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) 
    private readonly userModel: Model<UserDocument>
  ) {}

  async create(requestCreateUser: RequestCreateUserDto) {
    try {
      const user = await this.userModel.create(requestCreateUser);
      return user;
    } catch (e) {
      throw e;
    }
  }

  async findByEmail(email: string) {
    if (!email) {
      throw Error("Missing email in data");
    }
    const user = await this.userModel.findOne({ email });
    return user;
  }

  async findById(userId: string) {
    return await this.userModel.findById(userId);
  }
}