import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { BASE_STATUS } from "src/common/type";

export type UserDocument = HydratedDocument<User>;
@Schema({
  timestamps: true,
})
export class User {
  @Prop({
    required: true,
  })
    firstName: string;

  @Prop({
    required: true,
  })
    lastName: string;

  @Prop({
    unique: true,
    index: true,
  })
    email: string;

  @Prop({
    unique: true,
    index: true,
  })
    phoneNumber: string;

  @Prop()
    password: string;
  
  @Prop({
    default: BASE_STATUS.ACTIVE,
  })
    status: string;
}
export const UserSchema = SchemaFactory.createForClass(User);