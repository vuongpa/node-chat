import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/mongoose";
import { compare, hash } from "bcrypt";
import mongoose, { Model } from "mongoose";
import { TokenObject } from "./refresh-token.dto";
import { RefreshToken, RefreshTokenDocument } from "./refresh-token.schema";

const ObjectId = mongoose.Types.ObjectId;
@Injectable()
export class RefreshTokenService {
  constructor(
    @InjectModel(RefreshToken.name)
    private readonly refreshTokenModel: Model<RefreshTokenDocument>,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async getTokenObject(userId: string, refreshToken: string) {
    const accessToken = await this.jwtService.sign({ userId });
    const accessTokenExpiresIn = this.configService.get("JWT_ACCESS_TOKEN_EXPIRES_IN");
    return {
      accessToken,
      accessTokenExpiresIn,
      accessTokenExpiresAt: new Date().getTime() + accessTokenExpiresIn,
      refreshToken,
    };
  }

  async refreshAccessToken(userId: string, refreshToken: string): Promise<TokenObject> {
    const refreshTokenData = await this.refreshTokenModel.findOne({
      where: { id: userId },
    });
    const hashedRefreshToken = await hash(refreshToken, 10);
    if (!compare(hashedRefreshToken, refreshTokenData.hashedRefreshToken)) {
      throw Error("Wrong refresh token");
    }

    return await this.getTokenObject(userId, refreshToken);
  }

  async signRefreshToken(userId: string) {
    await this.refreshTokenModel.deleteMany({
      where: { userId },
    });
    const refreshToken = await this.jwtService.sign({ userId });
    const newRefreshToken = await this.refreshTokenModel.create({
      userId: new mongoose.Types.ObjectId(userId),
      hashedRefreshToken: await hash(refreshToken, 10),
    });
    
    return { newRefreshToken, refreshToken };
  }
}