export class TokenObject {
  accessToken: string;
  accessTokenExpiresIn?: string | undefined;
  accessTokenExpiresAt?: string | undefined;
  refreshToken?: string | undefined;
}