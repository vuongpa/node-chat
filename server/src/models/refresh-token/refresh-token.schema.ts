import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument } from "mongoose";
import { BASE_STATUS } from "src/common/type";
import { User } from "../user/user.schema";

export type RefreshTokenDocument = HydratedDocument<RefreshToken>;

@Schema({ 
  timestamps: true,
  expires: process.env.JWT_REFRESH_TOKEN_EXPIRES_IN,
})
export class RefreshToken {
  @Prop({ required: true })
    userId: mongoose.Schema.Types.ObjectId;

  @Prop({ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: "User", 
  })
    user: User;

  @Prop({ required: true })
    hashedRefreshToken: string;

  @Prop({
    default: BASE_STATUS.ACTIVE,
    enum: BASE_STATUS,
  })
    status: string;
}
export const RefreshTokenSchema = SchemaFactory.createForClass(RefreshToken);