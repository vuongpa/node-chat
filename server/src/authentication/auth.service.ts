import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { User } from "src/models/user/user.schema";
import { UserService } from "src/models/user/user.service";
import { RegisterDto } from "./dto/register.dto";
import { omit } from "lodash";
import { compare, hash } from "bcrypt";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}
  
  async hashPassword(password: string): Promise<string> {
    const passwordHash = await hash(password, 10);
    return passwordHash;
  }

  public async register(registrationData: RegisterDto) {
    const hashedPassword = await this.hashPassword(registrationData.password);
    try {
      const createdUser = await this.userService.create({
        ...registrationData,
        password: hashedPassword,
      });
      return omit(createdUser, "password");
    } catch (error) {
      throw error;
    }
  }

  public async getAuthenticatedUser(email: string, hashedPassword: string) {
    try {
      const user = await this.userService.findByEmail(email);
      if (!user) {
        throw Error(`Could not found user by email ${email}`);
      }
      
      const isPasswordMatching = this.verifyPassword(hashedPassword, user.password);
      if (!isPasswordMatching) {
        throw Error("Wrong credentials provided");
      }
      
      return omit(user, "password");
    } catch (error) {
      throw Error("Wrong credentials provided");
    }
  }

  public async verifyPassword(plainTextPassword: string, hashedPassword: string) {
    const isMatching = await compare(plainTextPassword, hashedPassword);
    return isMatching;
  }
}