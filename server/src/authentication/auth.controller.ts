import { Body, Controller, Get, HttpException, HttpStatus, Post, Req, UseGuards } from "@nestjs/common";
import { hash } from "bcrypt";
import { omit } from "lodash";
import { RefreshTokenService } from "src/models/refresh-token/refresh-token.service";
import { AuthService } from "./auth.service";
import { LoginDto } from "./dto/login.dto";
import { RegisterDto } from "./dto/register.dto";
import RequestWithUser from "./dto/request-with-user.dto";
import JwtAuthenticationGuard from "./jwt-auth.guard";

@Controller()
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly refreshTokenService: RefreshTokenService,
  ) {}

  @Post("/signup")
  async register(
  @Body() registrationData: RegisterDto
  ) {
    try {
      const user = await this.authService.register(registrationData);
      const refreshTokenData = await this.refreshTokenService.signRefreshToken(user.id);
      return await this.refreshTokenService.getTokenObject(user.id, refreshTokenData.refreshToken);
    } catch (error) {
      if (error?.code === 11000) {
        throw new HttpException("User with that email already exists", HttpStatus.BAD_REQUEST);
      }
      throw new HttpException("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post("/signin")
  async logIn(
  @Body() requestLogin: LoginDto
  ) {
    try {
      const hashedPassword = await hash(requestLogin.password, 10);
      const user = await this.authService.getAuthenticatedUser(requestLogin.email, hashedPassword);
      const refreshTokenData = await this.refreshTokenService.signRefreshToken(user.id);
      return await this.refreshTokenService.getTokenObject(user.id, refreshTokenData.refreshToken);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UseGuards(JwtAuthenticationGuard)
  @Get("/me")
  getMe(
  @Req() request: RequestWithUser
  ) {
    const user = request.user;
    return omit(user, "password");
  }
}