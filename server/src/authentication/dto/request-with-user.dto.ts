import { Request } from "express";
import { User } from "src/models/user/user.schema";
 
interface RequestWithUser extends Request {
  user: User;
}
 
export default RequestWithUser;