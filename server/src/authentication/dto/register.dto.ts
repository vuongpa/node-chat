import { MinLength } from "class-validator";
import { RequestCreateUserDto } from "src/models/user/user.dto";

export class RegisterDto extends RequestCreateUserDto {
  @MinLength(8)
    password: string;
}